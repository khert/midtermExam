/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam;

import java.util.HashMap;

/**
 *
 * @author staff
 */
public class Subject {
    private HashMap students = new HashMap();

    public Subject() {
    }

    public HashMap getStudents() {
        return students;
    }

    public void setStudents(HashMap students) {
        this.students = students;
    }
    
    public void addStudent(Student student){
        students.put(student.getIdNumber(),student);
    }
    
}
