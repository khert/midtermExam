/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam;

import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author staff
 */
public class MidtermExam {

    /**
     * @param args the command line arguments
     */
    public static HashSet<Student> students = new HashSet();
    public static Subject java = new Subject();
    public static LinkedList<Student> stud = new LinkedList<>();
    public static void main(String[] args) {
        // TODO code application logic here
        Student a = new Student(1123, "Khert", "Geverola");
        Student b = new Student(1124, "Ian John", "Baid");
        Student c = new Student(1125, "Fresca Joyce", "Olila");
        Student d = new Student(1126, "Fresca Joyce", "Zlila");
        Student e = new Student(1127, "Fresca Joyce", "Alila");
        
  
        addStudent(c);
        addStudent(a);
        addStudent(b);
        addStudent(d);
        addStudent(e);
        System.out.println("\n\n========== HASH MAP AREA =============");
        java.getStudents().forEach((k,v)->{
            System.out.println("Key: "+k+" Value: "+v.toString());
        });
        

    }
    public static void addStudent(Student student) {
        stud = new LinkedList<>();
        students.add(student);
     
        students.forEach((a) -> {
            stud.add(a);
        });
        stud.sort((Student a, Student b) -> a.getLastName().compareTo(b.getLastName()));
        stud.forEach((a) -> {
            students.add(a);
            System.out.println(a.toString());
        });
        java.addStudent(student);
        System.out.println("========================"); 
    }
}
