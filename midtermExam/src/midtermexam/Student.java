/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam;

/**
 *
 * @author staff
 */
public class Student {
    private int studentId;
    private String firstName;
    private String lastName;
  

    public Student() {
    }

    public Student(int idNumber, String firstName, String lastName) {
        this.studentId = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getIdNumber() {
        return studentId;
    }

    public void setIdNumber(int idNumber) {
        this.studentId = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return  lastName ;
    }
    
    

   
    
    
}
